
module.exports = (sequelize, DataTypes) => {
  const Shop = sequelize.define('Shop', {
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    supplierId: DataTypes.INTEGER
  }, {});
  Shop.associate = function(models) {
    // associations can be defined here
    Shop.BelongsTo(models.Supplier, {
      foreignKey: 'supplierId',
      as: 'supplier',
      onDelete: 'CASCADE',
    });
  };
  return Shop;
};