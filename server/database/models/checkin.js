
module.exports = (sequelize, DataTypes) => {
  const Checkin = sequelize.define('Checkin', {
    reason: DataTypes.STRING,
    note: DataTypes.STRING,
    supplierId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    shopId: DataTypes.INTEGER,
  }, {});
  Checkin.associate = function(models) {
    // associations can be defined here
    Checkin.BelongsTo(models.Supplier, {
      foreignKey: 'supplierId',
      as: 'supplier',
      onDelete: 'CASCADE',
    });
    Checkin.BelongsTo(models.User, {
      foreignKey: 'userId',
      as: 'salesman',
      onDelete: 'CASCADE',
    });
    Checkin.BelongsTo(models.Shop, {
      foreignKey: 'shopId',
      as: 'shop',
      onDelete: 'CASCADE',
    });
  };
  return Checkin;
};