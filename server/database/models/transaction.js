
module.exports = (sequelize, DataTypes) => {
  const Transaction = sequelize.define('Transaction', {
    name: DataTypes.STRING,
    price: DataTypes.NUMERIC,
    supplierId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER,
    shopId: DataTypes.INTEGER,
  }, {});
  Transaction.associate = function(models) {
    // associations can be defined here
    Transaction.BelongsTo(models.Supplier, {
      foreignKey: 'supplierId',
      as: 'supplier',
      onDelete: 'CASCADE',
    });
    Transaction.BelongsTo(models.User, {
      foreignKey: 'userId',
      as: 'salesman',
      onDelete: 'SET NULL',
    });
    Transaction.BelongsTo(models.Supplier, {
      foreignKey: 'shopId',
      as: 'shop',
      onDelete: 'SET NULL',
    });
  };
  return Transaction;
};