module.exports = (sequelize, DataTypes) => {
  const Product = sequelize.define('Product', {
    name: DataTypes.STRING,
    price: DataTypes.NUMERIC,
    supplierId: DataTypes.INTEGER
  }, {});

  Product.associate = function(models) {
    // associations can be defined here
    Product.BelongsTo(models.Supplier, {
      foreignKey: 'supplierId',
      as: 'supplier',
      onDelete: 'CASCADE',
    });
  };
  return Product;
};