
module.exports = (sequelize, DataTypes) => {
  const Report = sequelize.define('Report', {
    reason: DataTypes.STRING,
    note: DataTypes.STRING,
    supplierId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {});
  Report.associate = function(models) {
    // associations can be defined here
    Report.BelongsTo(models.Supplier, {
      foreignKey: 'supplierId',
      as: 'supplier',
      onDelete: 'CASCADE',
    });
    Report.BelongsTo(models.User, {
      foreignKey: 'userId',
      as: 'salesman',
      onDelete: 'CASCADE',
    });
  };
  return Report;
};