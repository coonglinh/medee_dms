
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    phone: DataTypes.STRING

  }, {});
  User.associate = function(models) {
    // associations can be defined here
    User.belongsToMany(models.Supplier, {
      through: models.UserSupplier,
    });
  };
  return User;
};